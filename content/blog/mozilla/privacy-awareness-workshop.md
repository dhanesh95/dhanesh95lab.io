---
title: Privacy Awareness Workshop
date: 2016-02-10
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
author: Dhanesh Sabane
description: A workshop to increase privacy awareness amongst students
categories: [Community]
---

## Event Details

**Name**: [Privacy Awareness Workshop at Zeal COE](https://reps.mozilla.org/e/privacy-awareness-at-zeal/)

**Agenda**:
* Privacy and Security talk
* Available addons in Firefox browser to help protect privacy
* Q&A

**Day and Date**: Saturday, 30th January 2016

**Venue**: ZCOER Computer Department, Seminar Hall, Room No. 415

**Speakers**:
* [Diwanshi Pandey](https://mozillians.org/en-US/u/diwanshipandey/)
* [Ankit Gadgil](https://mozillians.org/en-US/u/ankitgadgil/)
* [Prathamesh Chavan](https://mozillians.org/en-US/u/prathamesh/)


The 2016 new year marked the inception of the Mozilla India initiative - *["January Privacy Month Campaign"](https://wiki.mozilla.org/India/task_force/Policy_and_Advocacy/January_Privacy_Month_Campaign)*. It was an initiative to make the consumers of the Web aware about their privacy and security online. I was planning to float an idea to the club committee to arrange a privacy awareness workshop at our own college when Prathamesh contacted me with the same intent. After brainstorming for a couple of days, we came up with a date and also and agenda for the event.

The event began with a small introduction of all the Mozilla Reps. The attendees were also briefed about the event agenda. Diwanshi then took over with a small introduction as to what the Privacy Month campaign is all about. She also explained the motive behind the campaign. After the campaign introduction, she explained about user privacy and why should we as normal users should be considerate about our own privacy and security online.

![Diwanshi's Intro - 1](/images/mozilla/diwanshi_intro_1.jpg){:height="50%" width="50%"}

![Diwanshi's Intro - 2](/images/mozilla/diwanshi_intro_2.jpg){:height="50%" width="50%"}

Diwanshi's talk was followed by Ankit who had a large share of the event agenda. He started off with the tips which were shared online everyday. After that, he talked about what is tracking and the approaches which are used by the intruders to track online activity. He demonstrated how every activity is tracked by using the [Lighbeam](https://addons.mozilla.org/en-US/firefox/addon/lightbeam/?src=search) add-on. Some other add-ons which were discussed by Ankit were:

* [Ghostery](https://addons.mozilla.org/en-US/firefox/addon/ghostery/?src=ss)
* [Privacy Badger](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)
* [Adblock Plus](https://addons.mozilla.org/en-US/firefox/addon/adblock-plus/?src=ss)
* [Web of Trust](https://addons.mozilla.org/en-US/firefox/addon/wot-safe-browsing-tool/?src=ss)

Firefox is a browser with huge customization options. Some tweaks using **about:config** were also shared by Ankit.

![Firefox Tweaks by Ankit](/images/mozilla/paw_firefox_tweaks.jpg){:height="50%" width="50%"}

![Lightbeam Demo](/images/mozilla/paw_lightbeam_demo.jpg){:height="50%" width="50%"}

The final segment was about the Tor Network. Although it wasn't mentioned in the agenda, we took the liberty to talk about it for the sake of online security of the users. Ankit performed a small demonstration of the working of the Tor Network with the help of a few volunteers. After the demonstration, Prathamesh stepped up and talked about the Mozilla Pune Community. He enlightened he attendees about the several pathways they could adopt to be a part of the community and contribute to Mozilla.

![Ankit explaining the Tor Network](/images/mozilla/paw_tor_network.jpg){:height="50%" width="50%"}

![Prathamesh talking about Mozilla Pune Community](/images/mozilla/paw_community_intro.jpg){:height="50%" width="50%"}

The event concluded with a thank you note from me. Ankit had some pleasant surprises for everyone. He distributed a bunch of stickers to all the attendees. Selfies were clicked and everyone was merrier than ever. :D

![Group Photo 1](/images/mozilla/paw_group_1.jpg){:height="20%" width="50%"} ![Group Photo 2](/images/mozilla/paw_group_2.jpg){:height="50%" width="50%"}

<hr />

_Edit (2nd March 2018)_: It may be possible that some of the addons mentioned in the post may not be available - courtesy Firefox 57. Here's a list of some WebExtensions that can replace the functionality of the missing addons - https://dgtl.link/Firefox-Addons
