---
title: MozoGeek!
date: 2015-08-23
tags:
- Firefox
- FSA
- Mozilla
- Open Source
- Zeal Firefox Club
categories: [Community]
author: Dhanesh Sabane
description: Zeal Firefox Club Event - MozoGeek
---

## Event details:

- Name : Mozogeek!
- Agenda :
    1. Introduction to Firefox OS
    2. Firefox OS App making - Foxication
    3. Hands on training on Appmaker
- Day and Date : Saturday, 22nd August 2015
- Venue : Computer Department, ZES's Zeal College of Engineering and Research, Narhe, Pune
- Speaker : Prasad Seth
- Hashtag : #mozogeekzfc

----------------------------

After finishing with the in-semester examinations we were looking for a much needed break from the studies. So, what better way to get some amount of respite than to arrange an event! This event - MozoGeek! - was all about Firefox OS.

![MozoGeek](/images/mozilla/mozogeek-1.jpg)


In our earlier event, we had discussed with the members the possibility of collaboratively learning HTML5 and making some significant contributions in form of Firefox OS applications. In accordance to that, we decided to arrange an event exclusively on Firefox OS which led to the inception of this event.

![MozoGeek-FirefoxOS](/images/mozilla/mozogeek-firefoxos.jpg)


Our first choice as speaker was Prasad Seth as he has been an extremely active contributor and he has a huge experience in the application development field.
Some applications developed by Prasad Seth:

![MozoGeek-Prasad-Apps](/images/mozilla/mozogeek-prasad-apps.jpg)


Prasad began with a small introduction to Mozilla and it's missions.

![MozoGeek-Attendees](/images/mozilla/mozogeek-attendees-1.jpg)


![MozoGeek-Attendees-1](/images/mozilla/mozogeek-attendees-2.jpg)


Then he moved on to the introduction of Firefox OS. He explained the advantages of Firefox OS and why it is a great mobile OS.

After the introduction part, the students were asked to move to a lab to proceed with the hands on session.

The computers were updated to the latest versions of Firefox prior to the event by us. The students took their seats and again Prasad was incharge. He guided the students through the functionalities in AppMaker and the students responded very well. The club coordinators and volunteers helped the students with their doubts.

The event concluded on an inspirational note and the students were extremely excited to try out all the functions on AppMaker.

A huge thanks to Prasad Seth for his valuable guidance.

Love Firefox! <3

P.S : We dedicate this event to Nishigandha Yadav, our Club Lead. She had an unfortunate incident wherein she met with an accident just an hour before the event. We're extremely sorry for what she has been through and this event would have never happened without her guidance and motivation.
