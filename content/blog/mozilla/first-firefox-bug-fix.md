---
title: First bug fix for the Gecko engine
date: 2016-10-16
tags:
- Firefox
- FSA
- Mozilla
- Open Source
author: Dhanesh Sabane
description: A brief report of my first ever bug fix for the Gecko engine
categories: [Community]
---

After a host of non-technical contributions to Mozilla,it was time to switch to the technical side.  As a result, I started looking out for some bugs which I would be able to fix.  That is when I landed on [Bug 1308137](https://bugzilla.mozilla.org/show_bug.cgi?id=1308137) - Remove code around `IBMBIDI_SUPPORTMODE_*`.

The patch I provided was accepted today by [Xidorn Quan](https://www.upsuper.org/).

The process of fixing this bug was extremely informative and fun.  As I opened the files to make changes to the code I was greeted with very unfamiliar syntax.  Surely not the sort of code you'll ever encounter in four years of Computer Engineering.  I had to understand, even if at a very superficial level, what each snippet of code did in order to make the desired changes.  Fixing bugs in open source projects is definitely a test of your patience, determination and most importantly technical expertise.

I'm thankful to Xidorn for helping me with this bug. Looking forward to contribute more to the Mozilla code base.

Blaze your own path!
