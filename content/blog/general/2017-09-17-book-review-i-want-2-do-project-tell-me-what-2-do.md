---
title: Book Review - "i want 2 do project. tell me wat 2 do." by Shakthi Kannan
date: 2017-03-13
tags:
- Book
author: Dhanesh Sabane
description: Book Review
categories: [Review]
---

[Shakthi Kannan](http://shaktimaan.com/index.html) is a Free Software Developer whose book inspired me to contribute to the [Fedora Project](https://fedoraproject.org/wiki/Overview). His writing is descriptive, encouraging and unembellished. This is one of the books that you should keep around to help you whenever you need.

The title of the book refers to how students are accustomed to using SMS language which can harm them in business and professional situations. It was this kind of interaction with a student that inspired the author to assess situations in Indian institutions and help students improve themselves in all aspects. The book starts with the basics of how one should address people in a conversation to the “Art of Making Presentations” (addressing a huge audience) and maintaining oneself in a community. The author takes the reader on an informative journey through the well structured and inter-linked chapters.

The book comes in ten sections, each section addressing a particular aspect of a contributor’s life cycle. In practice this means that the reader gets to learn what to do and what not to do when faced by certain situations. This not only applies to situations that arise during Open Source contributions but also in day-to-day life. The book explains every section with examples and use cases which help in better understanding . Every section has a ‘References’ sub-section at the last which provides links to resources and references to books that the author recommends the reader to go through. This is a rich collection which can prove extremely helpful.

Now, this book has been written in 2013, and even with our rapidly changing lives, almost all it’s contents are still applicable. This is because the author has concentrated on tackling the basics which are hardly to change over time. But, they go a long way in shaping a person’s professional and personal lives. Although this book feels like a guide for people who want to contribute to Open Source projects, I like to look at it as a comprehensive guide to becoming a better programmer and a better person.

As I’ve said in the beginning of this review: You should always keep this book around to help you whenever you need (unless you have an eidetic memory). When I read the book, I had a better understanding of how communities function and how I should be approaching them. This helped me blend myself very well in every other community I became a part of.

Final verdict: **MUST READ**. There’s so much to be had from this book. And if all else fails, which will hardly be the case, you can still use it as a quote machine.

Buy it, read it, become a better human being.

![Book Cover](/images/general/shakthi_bookcover.png)

Excuse my poor selfie skills but this is the best I could come up with.

![Selfie with the book](/images/general/selfie_with_shakthi_book.webp)
