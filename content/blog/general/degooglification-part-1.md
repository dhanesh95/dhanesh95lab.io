---
title: De-Googlification - Part 1
date: 2016-12-02
tags:
- Open Source
- De-Googlification
author: Dhanesh Sabane
description: First part of the story of my attempts to remove every trace of Google from my digital life.
categories: [Privacy]
series: De-Googlification
---

> De-Googlification - To detach yourself from "Google's grip" on your Web life.

---
{{< center >}}
<h3>The story of my attempts to remove every trace of Google from my digital life. Let's start easy.</h3>
{{< /center >}}

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png" height=50% width=50% alt="Google 2015 logo.svg" attr="Google LLC, Public domain, via Wikimedia Commons" attrlink="https://commons.wikimedia.org/w/index.php?curid=42827827" >}}


Up until today, I've relied on Google services to meet my online computational needs. I own a [Moto E2](https://www.motorola.com/we/products/moto-e-gen-2) which is shipped with Android Lollipop as the operating system. I used Google Drive to store some data. I used YouTube to watch cat videos. I obviously used Gmail and you can never ignore Google Search. But this made me realize that one company has far too much control on my online life. And I'm sure a lot of people reading this article have had this realization. But we never acted on it, did we?

Roaming around in the walled gardens of Google is convenient, enjoyable but not particularly reliable. Especially when the company which adhered to a "[Don't be evil](https://en.wikipedia.org/wiki/Don%27t_be_evil)" ethos, is finding it difficult to stick to those principles.

If there's anything [CITIZENFOUR](https://citizenfourfilm.com/) has taught me, it's that you can never trust Google's services (Even Facebook as a matter of fact but more on that later). You've got to get back in control of your online life and stop being 'used'.

Thusly, I'm going to replace all the Google services in use with Free and Open Source software. Now this seems initimidating but I'm sure the end result will be pleasant and of digital 'freedom'.

So I'm jumping in. Head first.

Let us first identify a list of all the Google services I need to get rid of...

* Google Search

* Android

* Google Plus

* Gmail

* Google Docs

* Google Drive

* Google Maps

* YouTube

The process of taking back control has a pretty straight-forward start. The easiest thing I can get rid of is the search engine. And what better alternative to Google Search than [DuckDuckGo](https://duckduckgo.com/)?

DuckDuckGo is "The search engine that doesn't track you" and it is Open Source. There could not be a better match for me. Truthfully, I've been using DuckDuckGo for over a year now and I find myself very happy with the service. The best thing about it is that you can use "[Bangs](https://duckduckgo.com/bang)" to customize your search for specific sites and services. For example, *!w cats* will initiate a Wikipedia search for cats. You can even [customize](https://duckduckgo.com/settings) DuckDuckGo to your liking.

What more? DuckDuckGo is also a programmer-friendly search engine. The complete website is keyboard-navigable. Vim fans, particularly, will love to move around in the search results using the h, j, k and l keys.

So basically DuckDuckGo is a great service and gives me a solid start.

Meanwhile, I've been downloading CyanogenMod OS to be installed on my Moto E2. So by the time I finish my download, let's quickly take a look at the list again:

| Service       | Status      |
|---------------|-------------|
| Google Search | Replaced    |
| Android       | In progress |
| Google Plus   | Still here  |
| Gmail         | Still here  |
| Google Docs   | Still here  |
| Google Drive  | Still here  |
| Google Maps   | Still here  |
| YouTube       | Still here  |

1 down.  7 to go.

Okay, that was pretty quick and I'm not blessed with blazing fast internet speeds.  So if you'll excuse me I'll divert all my network resources to downloading the ROM and take a hiatus to think about other replacements.

See you in a while. ;-)
