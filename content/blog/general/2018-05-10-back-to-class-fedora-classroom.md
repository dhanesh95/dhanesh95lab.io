---
title: Back to class! - Fedora Classroom
date: 2018-05-10
tags:
- Fedora
- Open Source
author: Dhanesh Sabane
description: Introduction to the Fedora Classroom project.
categories: [Community]
---

# Introduction

Fedora Classroom is a project that aims at increasing awareness among the users about the distribution and understanding how to better use it. It started off ambitiously almost a decade ago and has now been revamped by the members of the [Fedora Join](https://fedoraproject.org/wiki/SIGs/Join) SIG.

Starting 28th July 2017, the revamped version of the project has successfully organized almost 6 classroom session on various topics like basics of FOSS, git, VIM, QA Testing and so on. Almost all these classes were held over [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) in the **#fedora-classroom** channel on [Freenode](https://irc.freenode.net).

# Help Wanted

The Fedora classroom project is always on the look out for people who can help out in any way possible. The Fedora Join SIG, which is currently running the Fedora Classroom project, is looking for people who can fit in and lend a hand with the operations. Have a look at the wiki page – [https://fedoraproject.org/wiki/Classroom\_Help\_Wanted](https://fedoraproject.org/wiki/Classroom_Help_Wanted) – and add in your name if the work interests you.

## See you on the other side!
