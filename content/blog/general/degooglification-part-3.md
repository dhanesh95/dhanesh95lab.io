---
title: De-Googlification - Part 3
date: 2016-12-25
tags:
- Open Source
- De-Googlification
author: Dhanesh Sabane
description: Third part of the story of my attempts to remove every trace of Google from my digital life.
categories: [Privacy]
series: De-Googlification
---
> De-Googlification - To detach yourself from "Google's grip" on your Web life.

---
{{< center >}}
<h3>A surprise alternative to Gmail and Google Drive emerges.</h3>
{{< /center >}}

{{< figure src="/images/general/disroot_logo.png" height=50% width=50% alt="Disroot Logo" attr="To tear up the roots of, or by the roots; hence, to tear from a foundation; to uproot." attrlink="https://disroot.org">}}

After a few easy replacements of Google services, I arrived at a difficult junction in this journey. The thought of replacing Gmail and Google Drive was frightful.  Honestly, Gmail is a solid, reliable(mostly) service which handles tens and thousands of emails effortlessly. Plus, you don’t have to pay anything to use it. That’s always nice too. But the fact that it mines my data to provide advertisements in my mailbox is very disturbing and was not going to fly any more.

Now, I had two distinct types of options that most of us would consider when trying to find an alternative to GMail:

1. Setting up my own email server.
2. Selecting another email service to put my trust, and data, in.

As I mentioned in my previous post, setting up my own server is not my cup of tea. So, the first option is down the gutter  Turning to the second option, I was looking for services which would be an improvement in some of Google’s weakest areas. For instance, no advertisements and security and privacy had to be paramount.

After consulting with Privacy Tools website I found ProtonMail and Tutanota to be some solid alternatives. However, the storage space they provide – 500MB and 1GB, respectively -is not enough for me. Also the one-account-for-all-services experience, which I was familiar with, was missing. Even so, I’ve opened an account on both of the services.

### Enter [Disroot](https://disroot.org).

Disroot is based out of Amsterdam, Netherlands and is maintained by a bunch of friendly group of volunteers. Disroot’s [philosophy](https://disroot.org/about/) resonates with mine and it attracted me to it like a moth to a flame! This service, I believe, is the only solution to Gmail, Google Drive, Google Contacts and Google Calendar. Apart from that, it also provides a [host of other services](https://apps.disroot.org/) which come at handy on different occasions. All these services it provides are based on the [NextCloud](https://www.nextcloud.com/) server, a fork of OwnCloud.

The setup for Disroot was nice and easy – as you’d expect.  The webmail experience is excellent – although I needed some time to adjust to the new layout.  Once everything was up and running, I immediately logged in to my Gmail account and turned on the forwarding of all emails to my new Disroot email address.  It will take me a while before I’ve changed all my accounts on the various websites strewn across the internet to my new email account.  But the process has begun. On my mobile device, I’m using the native email client to access my Disroot Mail using IMAP which works like a charm.

Apart from mail, Disroot also provides contact sync, calendar sync and cloud storage.  I use [DAVdroid](https://davdroid.bitfire.at/) to sync all my contacts and calendar with my native apps.  To access cloud storage, there is an official NextCloud mobile client available for use.

Google provides a hefty 15GB of storage for Email and the contents on your Drive which is not the case with Disroot.  It provides 4GB storage which is more than enough for a free service which doesn’t sell your data or profile you to figure out how to more effectively advertise to you.

> EDIT (21st June 2020): Disroot now only provides 1 GB of storage to free users. The signup process has also been refined to avoid spam.

All-in-all I’m fairly excited about this change.  Sure, I simply traded dependence on Google for dependence on Disroot.  But in that trade, I will also get some significant improvements in privacy and open source.

Also according to recent developments, Disroot has joined the Matrix network!  Matrix is the future of online communication – the way we interact with people – and Disroot being a part of it makes me love the service even more.  It amazes me that how this service is continuously striving to provide the best of services to its users and still doesn’t charge a dime.  And so I pledge to myself on this day:

> **As soon as I start earning, my first donation will be towards Disroot and if possible, I’ll make it a yearly affair.**

> EDIT (21st June 2020): Disroot was a Matrix provider but they discontinued the service in 2018 - [Matrix closure](https://disroot.org/en/blog/matrix-closure).

I encourage the readers to make use of this awesome service and if you can afford it, please [donate](https://disroot.org/donate/) to keep the project going.

So now I can cross two more items off my list:

| Service       | Status      |
|---------------|-------------|
| Google Search | Replaced    |
| Android       | Replaced    |
| Google Plus   | Replaced    |
| Gmail         | Replaced    |
| Google Docs   | Still here  |
| Google Drive  | Replaced    |
| Google Maps   | In progress |
| YouTube       | Still here  |

5 down. 3 to go.
