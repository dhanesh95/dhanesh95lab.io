---
title: De-Googlification - Part 4
date: 2020-08-30
tags:
- Open Source
- De-Googlification
author: Dhanesh Sabane
description: Fourth and final part of the story of my attempts to remove every trace of Google from my digital life.
categories: [Privacy]
series: De-Googlification
---
> De-Googlification - To detach yourself from "Google's grip" on your Web life.

---
{{< center >}}
I finally found my way out of the walled garden.
{{< /center >}}
<br />

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Osmand_logo.svg/256px-Osmand_logo.svg.png" height=25% width=25% alt="OSMAnd logo" attr="https://github.com/osmandapp/, Public domain, via Wikimedia Commons" attrlink="https://github.com/osmandapp/">}}

It's been some time since I posted the third edition of this [De-googlification series](/series/de-googlification/) (Almost three years and nine months! :O). A couple of days ago, one of the fellow [FSCI community](https://fsci.in) members shared a blog post that he wrote about de-googling your Android phone and it reminded me that I had completely abandoned this series and never finished it! Well, as they say - _better late than never_.

So I had three more Google services to replace and I'll go through them one by one.

### Google Maps

This is a big one. Almost every smartphone owner in this world has Google Maps on their device. It does it's job perfectly and is a very tricky one to replace if you are on a de-googlification journey. I'll admit it, Google Maps has brought me out of situations where I took a wrong turn down a street and found myself in middle of nowhere. I was a bit hesitant to stop using Google Maps and tried to use it without signing in with a Google account. But that didn't do justice to the mission I embarked on and so with a heavy heart, I decided to replace it. I had multiple options such as - HERE Maps, MAPS.ME, Yandex Maps but I chose to use [OsmAnd](https://osmand.net/) which used the high quality [OpenStreetMap](https://www.openstreetmap.org/) data. So far, it has worked perfectly for me and I've also relearned to be a social animal and not shy away from asking passers-by for directions.

### YouTube

Let's be honest with this one. There is absolutely no way one can completely ignore YouTube and switch to a different platform. However, what we can do is not use the YouTube **app** and replace it with a different **front-end**, if you may. So, I chose the following alternatives to the YouTube app:

1. [NewPipe](https://newpipe.schabi.org/) for Mobile
2. [Invidious](https://github.com/iv-org/invidious) for Desktop
    * There are multiple instances of Invidious and you can find a list on [Invidious documentation](https://docs.invidious.io/instances/).

None of these alternatives require you to sign in with a Google account, you can import all your subscriptions from YouTube and you can also download YouTube videos without having to worry about installing any browser plugins or video downloading software. FOSS for the win!

### Google Docs

As far as I'm aware, there are no drop in replacements for Google Docs but [Etherpad](https://etherpad.org/) did serve all of my collaborative document editing needs. It's simple and there are a huge number of Etherpad service providers, one of them being Disroot (remember them from my [third part](/blog/general/degooglification-part-3/) of the series?).

And with that I've replaced all the Google services that I had listed down thus de-googling my phone and essentially my life. For me, it feels like I've broken out of handcuffs and it gives me a sense of relief that I've made Google's job of tracking me, a bit harder (hopefully?).

| Service       | Status      |
|---------------|-------------|
| Google Search | Replaced    |
| Android       | Replaced    |
| Google Plus   | Replaced    |
| Gmail         | Replaced    |
| Google Docs   | Replaced    |
| Google Drive  | Replaced    |
| Google Maps   | Replaced    |
| YouTube       | Replaced    |
