---
title: About
draft: false
layout: single
---
You’re here, so I guess it would be rude not to introduce myself. ;-)


My name is Dhanesh B. Sabane and I am passionate about everything computers and the [Free and Open Source (FOSS) philosophy](https://en.wikipedia.org/wiki/Free_and_open-source_software).


### So, what kind of person am I?

Just like everyone else, I am a hard worker and I try not to take myself too seriously. I'm also a bit shy and a terrible basketball player. If you were to ask me to aim for the basket, I'd probably hit the guy standing ten feet away, talking on the phone.


### But it's not all bad

What I lack in basketball skills, I more than make up for in computer skills. A career Software Engineer, I'm currently spending my time as a Technical Lead at [Rentr](https://rentr.co/).


You'll usually find me spending my time with my devices on a weekend - breaking, fixing and tweaking stuff. And if not that, you'll find me at some FOSS conference talking about stuff I broke, fixed and tweaked last weekend.

_Such a nerd! - I hear you say under your breath._

### So, how did I get here?

My parents gifted me my first computer when I was a 13 year old kid. I had no idea about any of the Computer Science fundamentals up until I joined college with Computer Science as a subject. That's when I realised that logic and coding came naturally to me.


Later I pursued Computer Engineering and that is where I started realizing my true potential (it took me a while). I started attending events, workshops and hackathons. I wasn't the best one around but I never gave up. I started using and contributing to Open Source software and managed a Mozilla Club in my last year of graduation.

### And that's how the journey began

Currently, apart from my day job, I contribute towards FOSS projects, communities and any other project that interests me on the fly.

### Hang with me for a bit!

You can help me get over my shyness. Here are a few ways to make that happen:
- Make sure you're reading the [blog](/blog)!
- [Fediverse](https://fe.disroot.org/@dhanesh95) is fun too!!
- If you mean business, [LinkedIn](https://www.linkedin.com/in/dhaneshsabane/) is good.
- Or hit me up on one of the [IMs](/about/im) I use.


Thanks! I really appreciate you stopping by and hanging with me!

Have a good day! :)
